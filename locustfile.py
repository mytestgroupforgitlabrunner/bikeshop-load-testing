from locust import HttpUser, task

class HomePageUser(HttpUser):
    @task
    def test_home_page(self):
        self.client.get("/")
